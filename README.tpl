# {{crate}}

[![Build Status](https://gitlab.com/rusty-binder/rusty-cheddar/badges/master/build.svg)](https://gitlab.com/rusty-binder/rusty-cheddar/builds)
[![crates.io](http://meritbadge.herokuapp.com/rusty-cheddar)](https://crates.io/crates/rusty-cheddar)
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](http://mit-license.org/)

**This project is part of a major rewrite of
[rusty-cheddar](https://github.com/Sean1708/rusty-cheddar) with an aim to make a more
language-agnostic bindings generator. Until rusty-binder is put on `crates.io` it should be
considered entirely work-in-progress and unusable. Thanks for your patience!**

{{readme}}

## Contributing

Contributions to rusty-cheddar are more than welcome.

### Bugs

If you find a bug or have a feature request please open an issue. I can't guarantee that I'll fix it
but I'll give it a damn good go.

If you find the source code unclear in any way then I consider that a bug. I try to make my source
code as clear as possible but I'm not very good at it, so any help in that regard is appreciated.

### PRs

I love pull requests, they tend to make my job much easier, so if you want to fix a bug or implement
a feature yourself then that would be great. If you're confused by anything or need some pointers on
how to proceed then feel free to open an issue so that I can help, otherwise [these docs] are a good
place to start.

This project uses [clog] so please make sure your commit messages adhere to [conventional format],
thanks!

[these docs]: http://manishearth.github.io/rust-internals-docs/syntax/ast/index.html
[clog]: https://github.com/clog-tool/clog-cli
[conventional format]: https://github.com/ajoslin/conventional-changelog/blob/a5505865ff3dd710cf757f50530e73ef0ca641da/conventions/angular.md
